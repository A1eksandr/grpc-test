FROM scratch

ENV PORT 3000

EXPOSE $PORT

COPY ./bin/grpc-test /

CMD ["/grpc-test"]