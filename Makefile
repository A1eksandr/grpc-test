APP=grpc-test
PORT=3000
RELEASE=0.0.1
COMMIT=$(shell git rev-parse --short HEAD)
BUILD_TIME=$(shell date -u '+%Y-%m-%d_%H%M%S')
GOOS=linux
GOARCH=amd64
PROJECT=bitbucket.org/A1eksandr

.PHONY: restore
restore:
	gb vendor restore

.PHONY: clean
clean:
	rm -f ./bin/${APP}

.PHONY: build
build: clean
	CGO_ENABLED=0 GOOS=$(GOOS) GOARCH=$(GOARCH) gb build \
		-ldflags "-s -w -X ${PROJECT}/${APP}/version.BuildTime=${BUILD_TIME} \
		-X ${PROJECT}/${APP}/version.Commit=${COMMIT} \
		-X ${PROJECT}/${APP}/version.Release=${RELEASE}" ;\
	mv ./bin/${APP}-${GOOS}-${GOARCH} ./bin/${APP}

.PHONY: container
container: build
	docker build -t $(APP):v$(RELEASE) .

.PHONY: run
run: container
	docker stop $(APP):v$(RELEASE) || true && docker rm $(APP):v$(RELEASE) || true
	docker run --name ${APP} -p ${PORT}:${PORT} --rm -e "SERVER_PORT=${PORT}" $(APP):v$(RELEASE)
